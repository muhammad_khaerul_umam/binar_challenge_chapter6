//index.js
const express = require('express');



const app = express();

app.set('view engine','ejs');
app.set('views', __dirname + '/views');

app.get ('/',(req, res) => {
    //menampilkan halaman crud_user_ejs
    res.render('crud_user.ejs');
});

app.get ('/add',(req, res) => {
    //menampilkan halaman add user
    res.render('add.ejs');
});

app.get ('/:id',(req, res) => {
//menampilkan detail 

})

app.post ('/',(req, res) => {
    //membuat sebuah data
    
    })

app.put('/:id', (req, res) => {
//mengedit data
})

app.delete('/:id', (req, res) => {
//menghapus data
})

app.listen(8000, () => {
    console.log('aplikasi sudah berjalan');
});
